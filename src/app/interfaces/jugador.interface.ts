export interface Ijugador {
    rut: string;
    nombre: string;
    posicion: number;
    nacionalidad: string;
}