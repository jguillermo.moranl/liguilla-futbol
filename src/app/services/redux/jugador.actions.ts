import { createAction, props } from '@ngrx/store';
import { Ijugador } from 'src/app/interfaces/jugador.interface';

export const AgregarJugador = createAction('[Jugador Component] AgregarJugador', props<Ijugador>());
export const ActualizarJugador = createAction('[Jugador Component] ActualizarJugador');
export const EliminarJugador = createAction('[Jugador Component] EliminarJugador');