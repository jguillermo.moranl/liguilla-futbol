import { createReducer, on, State } from '@ngrx/store';
import { ActualizarJugador, AgregarJugador, EliminarJugador } from './jugador.actions';
import { Ijugador } from '../../interfaces/jugador.interface';

export const initialState: Ijugador = { nacionalidad: '', nombre: '', posicion: 0, rut: '' };

const _JugadorReducer = createReducer<Ijugador>(initialState,
    on(AgregarJugador, (state, { nacionalidad, rut, posicion, nombre }: Ijugador) => ({ nacionalidad, rut, posicion, nombre })),
    /*     on(ActualizarJugador, state => state - 1),
        on(EliminarJugador, state => 0), */
);

export function JugadorReducer(state, action) {
    return _JugadorReducer(state, action);
}