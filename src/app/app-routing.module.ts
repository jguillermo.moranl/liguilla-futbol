import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JugadoresComponent } from './components/jugadores/jugadores.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'jugadores' },
  { path: 'jugadores', component: JugadoresComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
