import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Ijugador } from 'src/app/interfaces/jugador.interface';
import { Observable } from 'rxjs';
import { AgregarJugador } from 'src/app/services/redux/jugador.actions';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.component.html',
  styleUrls: ['./jugadores.component.scss']
})
export class JugadoresComponent implements OnInit {

  jugador$: Observable<Ijugador>;
  constructor(private store: Store<{ jugador: Ijugador }>) { }

  ngOnInit(): void {
    this.jugador$ = this.store.pipe(select('jugador'));
  }

  nuevoJugador = () => {
    console.log('nuevo jugador');

    this.store.dispatch(AgregarJugador({ nombre: 'Jorge Gonzales', posicion: 9, rut: '20060520-2', nacionalidad: 'chilena' }));
  }


  get jugador(): Observable<Ijugador> {
    return this.jugador$;
  }

}
